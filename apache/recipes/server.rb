#
# Cookbook Name:: apache
# Recipe:: server
#
# Copyright (c) 2017 The Authors, All Rights Reserved.
# notifies :action, 'resource[name]', :timer
# subscribes :action, 'resource[name]', :timer
# 3 types of timer:  :before :delayed :immediately
# subscribe: 

package "httpd" do
	action :install
end

remote_file "/var/www/html/robin.png" do
	source "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1506055809312&di=10d74342c03c51d470e70918977c2201&imgtype=0&src=http%3A%2F%2Fup.qqjia.com%2Fz%2F24%2Ftu29253_9.jpg"
end

bash "inline script" do
	user "root"
	code "mkdir -p /var/www/mysites/ && chown -R apache /var/www/mysites"
#	not_if "[ -d /var/www/mysites/]"
	not_if do 
		File.directory?('/var/www/mysites/')
	end 
end

template "/var/www/html/index.html" do
	source "index.html.erb"
	action  :create
	#notifies :restart, 'service[httpd]', :immediately
end

service "httpd"  do
	action [ :enable, :start ]
	subscribes :restart, 'template[/var/www/html/index.html]', :immediately
end
