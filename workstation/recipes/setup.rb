package "vim-enhanced"
package "nano"
package "emacs"

package "tree" do 
	action :install
end 

package "ntp" 

package "git" do
	action :install
end 

node["ipaddress"] 
node["memory"]["total"]

# print statement "i have 4 apples"
apple_count=4
puts "I have #{apple_count} apples"

template "/etc/motd" do
	source "motd.erb"
	action :create
	variables(
		:name => "tech trainer"
	)
end
service "ntpd" do
	action [:enable, :start]
end

user 'user1' do 
	comment "user1"
	uid "123"
	home "/home/user1"
	shell "/bin/bash"
end

group "admin" do
	members ["user1"]
	append true
end
